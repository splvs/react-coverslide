import React from "react"

import slideStyle from "./slide.module.css"

/*
Slide Component (base)
Creates a page-covering element, styles can be overriden with style prop.
*/

// might be interesting as well: https://github.com/qmixi/react-full-height
export default function Slide(props) {
	var style;
	if ('backgroundColor' in props) {
		style = {backgroundColor: props.backgroundColor};
	}
	if ('backgroundImage' in props) {
		style = {...style, backgroundImage: props.backgroundImage};
	}
	return (
		<React.Fragment>
			<div style={style} className={slideStyle.slide}>
				{props.children}
			</div>
		</React.Fragment>
	);
}
