# react-coverslide
This is a tiny react component that covers the full viewport, used primarily for
our GatsbyJS websites.
It's also my (@fnetx's) first try with react components / libraries.

## Contributing
Helping hands are very welcome, I'd appreciate feedback very much, especially 
when it comes to accessibility and performance improvements.

Also I'm not sure if my way of doing things is the typical React way. Probably
not, so please reach out when you notice something odd.

## Install
I do not think I'll put this on npm anytime soon, so directly install it from 
this Codeberg repo.

## Roadmap
This slide is to be used by our GatsbyJS web projects. It should cover the
following tasks, although I didn't investigate if they are all doable so far:
- [x] provide a way to manually specify slide background
- [ ] provide image slides
- [ ] provide colour slides
- [ ] provide a way to fade between slides while scrolling (e.g. from an image to
	a color). While this is already possible with nesting slides (will probably
	be the first implementation), it should be made more flexible.
	also see https://stackoverflow.com/questions/66855596/overlapping-sticky-elements-but-content-keeps-default-scrolling
- [ ] include Gatsby's way of including (fluid) background images, preloading them
	etc
- [ ] provide anchoring of slides so that a menu bar could scroll to a specific
	slide in one pagers
- [ ] provide accessibility tools for specifying an optional background
	description (not displayed but visible to screen readers) and may be a neat
	way for navigating between slides

## Usage
Please have a look at the shipped example app. It covers the usage.

You can run `npm run start` both in the repo root and in the example subfolder.

## License
Unlicense © [fnetX](https://github.com/fnetX)
