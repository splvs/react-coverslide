import React from 'react'

import { Slide } from '@splvs/react-coverslide'
import '@splvs/react-coverslide/dist/index.css'

const App = () => {
	return (
		<>
			<Slide backgroundImage="url(https://source.unsplash.com/featured/?nature,water)">
				<h1>The react-coverslide</h1>
				<h2>creates slides with images ...</h2>
			</Slide>
			<Slide backgroundColor="#fab">
				<h2>... and colours ...</h2>
			</Slide>
			<Slide backgroundImage="url(https://source.unsplash.com/featured/?nature)">
				<h2>... that cover the full viewport.</h2>
			</Slide>
		</>
	)
}

export default App
